(server-start)
;; In console use the alias
;; "ec" for emacs-client, and "em" for emacs

;;;;;;;;;; Package system
(require 'package)
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")
			 ("org" . "http://orgmode.org/elpa/")))

;; (setq package-enable-at-startup nil)
(package-initialize)
(global-set-key (kbd "C-x p") 'package-list-packages)
(global-set-key [f6] 'find-file-in-repository)

;; (add-to-list 'load-path "~/.emacs.d") 
(add-to-list 'load-path "~/.emacs.d/lisp")
(setq default-directory "~/"); may override $HOME for file browse

;; basic settings: general aspects of emacs
(load "init_basic_settings")

;; OS-specified keybindings
(load "init_keybindings")

;; Language-specified modes
(load "init_modes")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(show-paren-mode t)
 '(tool-bar-mode nil))

;; (custom-set-faces
;;  ;; custom-set-faces was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(default ((t (:inherit nil :stipple nil :background "White" :foreground "Black" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 100 :width normal :foundry "nil" :family "Menlo")))))

;; (set-default-font "-*-Menlo-normal-normal-normal-*-12-*-*-*-m-0-iso10646-1")
