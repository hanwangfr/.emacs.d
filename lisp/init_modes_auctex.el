;;;;;;;;;;;;;;; Auctex (Not necessary for Aquamacs)
(setenv "PATH" (concat "/usr/texbin" ":" (getenv "PATH"))) ;; environment problem: /usr/texbin is the path of latex command that must be manually loaded if emacs is started from the docker
(setq exec-path (append '("/usr/texbin" "/usr/local/bin") exec-path))

(load "auctex.el" nil t t)
; (load "preview-latex.el" nil t t)
(require 'tex-site)
; (setq TeX-view-program-selection '((output-pdf "Preview"))) 

;; Fix the bug in error message display
(setq LaTeX-command-style '(("" "%(PDF)%(latex) -file-line-error %S%(PDFout)")))

(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)

;; (add-hook 'LaTeX-mode-hook 'visual-line-mode)
;; (add-hook 'LaTeX-mode-hook 'flyspell-mode)
;; (add-hook 'LaTeX-mode-hook 'latex-math-mode)
;; (add-hook 'LaTeX-mode-hook 'turn-on-reftex)

(setq reftex-plug-into-AUCTeX t) ;; This will assign default value to each new equation's label, use the follows instead. Comment this to turn off this property.

(add-hook 'LaTeX-mode-hook
      (lambda ()
        (visual-line-mode t)
        (reftex-mode t)
        (flyspell-mode t)
	(latex-math-mode t)
	)
      )

; (setq TeX-PDF-mode t)
					
;; Use Skim as viewer, enable source <-> PDF sync
;; make latexmk available via C-c C-c
;; Note: SyncTeX is setup via ~/.latexmkrc (see below)
;; (add-hook 'LaTeX-mode-hook (lambda ()
;;   (push
;;     '("latexmk" "latexmk -pdf %s" TeX-run-TeX nil t
;;       :help "Run latexmk on file")
;;     TeX-command-list)))
;; (add-hook 'TeX-mode-hook '(lambda () (setq TeX-command-default "latexmk")))

;; use Skim as default pdf viewer
;; Skim's displayline is used for forward search (from .tex to .pdf)
;; option -b highlights the current line; option -g opens Skim in the background  
(setq TeX-view-program-selection '((output-pdf "PDF Viewer")))
(setq TeX-view-program-list
     '(("PDF Viewer" "/Applications/Skim.app/Contents/SharedSupport/displayline -b -g %n %o %b")))

;; Sets the default PDF viewer to, well, the default PDF viewer.
;; (setq TeX-view-program-list '(("Shell Default" "open %o")))
;; (setq TeX-view-program-selection '((output-pdf "Shell Default")))

