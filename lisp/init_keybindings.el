;;;;;;;;;;;;;; Global key bindings
;; (global-set-key (kbd "C-c C-\\") 'comment-or-uncomment-region-or-line)

(if (string-equal system-type "darwin")   ; Mac OS X
  (progn
    ; (message "Load init_keybindings_osx.el")
    (load "init_keybindings_osx")
    )

  ;; else ((or (string-equal system-type "windows-nt") (string-equal system-type "gnu/linux")) ; Microsoft Windows or Linux
  (progn
    (load "init_keybindings_windows_linux")
    )
  )

