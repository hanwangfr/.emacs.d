;;;;;;;;;;;;;;;; Important modes and programming-related major modes

;;;;;;;;;;;;;;;;;;; Shell environment
(when (memq window-system '(mac ns))
  (exec-path-from-shell-initialize))

;;;;;;;;;;;;;;;;;;; Abbreviation mode, see: http://www.emacswiki.org/emacs/AbbrevMode
;; Something is wrong with python mode if abbrev-mode is on: if/for skeleton auto-insert.
;; (setq default-abbrev-mode t)
;; ;; (load "abbrev_defs")
;; (setq abbrev-file-name             ;; tell emacs where to read abbrev
;;       "~/.emacs.d/abbrev_defs.el")    ;; definitions from...

;;;;;;;;;;;;;;; iBuffer
(global-set-key (kbd "C-x C-b") 'ibuffer)
(autoload 'ibuffer "ibuffer" "List buffers." t)
(defalias 'list-buffers 'ibuffer) ; always use ibuffer

;;;;;;;;;;;;;;;;;;;; Ido mode
(require 'ido)
(ido-mode t)

;;;;;;;;;;;;;;;;;;; Tabbar
;; For pure emacs on osx/windows/linux
(require 'tabbar)
(tabbar-mode t)
;; http://www.emacswiki.org/emacs/TabBarMode

(if (string-equal system-type "darwin")   
    (progn ; For pure emacs on Mac OS X
      (global-set-key (kbd "s-{") 'tabbar-backward-tab) ; Default behavior of switching tabs in osx
      (global-set-key (kbd "s-}") 'tabbar-forward-tab)
      (global-set-key (kbd "s-[") 'tabbar-backward-group)
      (global-set-key (kbd "s-]") 'tabbar-forward-group)
      )
  (progn ; For pure emacs on windows/linux
    (global-set-key (kbd "M-{") 'tabbar-backward-tab)
    (global-set-key (kbd "M-}") 'tabbar-forward-tab)
    (global-set-key (kbd "M-[") 'tabbar-backward-group)
    (global-set-key (kbd "M-]") 'tabbar-forward-group)
    )
  )

;;;;;;;;;;;;;;;;;; Auctex
(load "init_modes_auctex")	

;;;;;;;;;;;;;;;;;; Python
(load "init_modes_python")
;; (pyenv-mode)

;; ;;;;;;;;;;;;;;;;;;;;;; CEDET
;; (global-ede-mode 1)                      ; Enable the Project management system
;; (semantic-load-enable-code-helpers)      ; Enable prototype help and smart completion 
;; (global-srecode-minor-mode 1)            ; Enable template insertion menu


;; ;; ;;;;;;;;;;;;;;;;;;; Smooth scroll (!!!!!!!!!!!!!!!!!!!!!!DISABLED since this slows down emacs)
;; (setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time    
;; (setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
;; ;; (setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
;; (setq scroll-step 1) ;; keyboard scroll one line at a time

;; (setq redisplay-dont-pause t
;;   scroll-margin 1
;;   scroll-step 1
;;   scroll-conservatively 10000
;;   scroll-preserve-screen-position 1)

;; ;;;;;;;;;;;;;;;;;;White space mode
;; make whitespace-mode use just basic coloring
;; (require 'whitespace)
;; ;; (global-whitespace-mode 1)
;; ;; (setq whitespace-style (quote (spaces tabs newline space-mark tab-mark newline-mark)))
;; ;; (setq show-trailing-whitespace 1)
;; ; (global-set-key (kbd "C-c C-w") 'whitespace-mode)
;; (setq whitespace-line 1)
;; (setq whitespace-space 1)

;;;;;;;;;;;;;;;;;smart tab
(require 'smart-tab)
;; (global-smart-tab-mode 1)

(global-set-key (kbd "C-<tab>") 'smart-tab)

;;;;;;;;;;;;;;;;;; recentfile mode
(recentf-mode 1) ; keep a list of recently opened files
;; set F7 to list recently opened file
(global-set-key (kbd "<f7>") 'recentf-open-files)

;; ;;;;;;;;;;;;;;;;;;;;;;; Flyspell
;; (eval-after-load "flyspell"
;;   '(progn
;;      (define-key flyspell-mouse-map [down-mouse-3] #'flyspell-correct-word)
;;      (define-key flyspell-mouse-map [mouse-3] #'undefined)))

;; ;;;;;;;;;;;;;;;;;;;;;;; Flycheck
(require 'flycheck)
(global-flycheck-mode t)

;; ;;;;;;;;;;;;;;;;;;;;;; Magit
(require 'magit)
(global-set-key (kbd "C-x g") 'magit-status)

;; (setq magit-last-seen-setup-instructions "1.4.0")

;;;;;;;;;;;;;;;;;;;; Programming
(require 'autopair) ;; parathesis
(require 'yasnippet) ;; code template

;;;;;;;;;;;;;;;;;;;; Auto-complete
(require 'auto-complete)
(require 'auto-complete-config)
(ac-config-default)

; auto-complete mode extra settings
(setq
 ac-auto-start 2
 ac-override-local-map nil
 ac-use-menu-map t
 ac-candidate-limit 20)

;;;;;;;;;;;;;;;;;;; Org-mode
(require 'org)
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-iswitchb)
;; (define-key global-map "\C-cl" 'org-store-link)
;; (define-key global-map "\C-ca" 'org-agenda)
;; (global-set-key "\C-cc" 'org-capture)
;; (global-set-key "\C-cb" 'org-iswitchb)

(setq org-log-done t)

;;;;;;;;;;;;;;;;;; CUA mode
;; (cua-mode t)

;;;;;;;;;;;;;;;;;; Column marker
(require 'column-marker)
;; Highlight column 80 in foo mode:
(add-hook 'python-mode-hook (lambda () (interactive) (column-marker-1 80)))
