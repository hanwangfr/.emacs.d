;; General keybindings for Mac

;; (setq mac-control-modifier 'control)
(setq mac-option-modifier 'meta)
(setq mac-right-option-modifier nil)
(setq mac-right-command-modifier 'control)

;; (setq mac-command-modifier nil)
;; (global-set-key [kp-delete] 'delete-char) ;; sets fn-delete to be right-delete

;; For pure Emacs only
;; Switch between windows      
(setq windmove-wrap-around t)
(global-set-key (kbd "s-<home>") 'windmove-left)          ; move to left windnow
(global-set-key (kbd "s-<end>") 'windmove-right)        ; move to right window
(global-set-key (kbd "s-<prior>") 'windmove-up)              ; move to upper window
(global-set-key (kbd "s-<next>") 'windmove-down)          ; move to downer window
;; (global-set-key (kbd "s-<left>") 'windmove-left)          ; move to left windnow
;; (global-set-key (kbd "s-<right>") 'windmove-right)        ; move to right window
;; (global-set-key (kbd "s-<up>") 'windmove-up)              ; move to upper window
;; (global-set-key (kbd "s-<down>") 'windmove-down)          ; move to downer window

;; Edit
;; (global-set-key (kbd "M-<up>") 'beginning-of-visual-line)
;; (global-set-key (kbd "M-<down>") 'end-of-visual-line)
(global-set-key (kbd "M-<up>") 'backward-sentence)
(global-set-key (kbd "M-<down>") 'forward-sentence)
(global-set-key (kbd "M-<left>") 'left-word)
(global-set-key (kbd "M-<right>") 'right-word)

(global-set-key (kbd "C-<left>") 'back-to-indentation)
(global-set-key (kbd "C-<right>") 'move-end-of-line)

(global-set-key (kbd "s-<left>") 'move-beginning-of-line)
(global-set-key (kbd "s-<right>") 'move-end-of-line)
(global-set-key (kbd "s-<up>") 'beginning-of-buffer)
(global-set-key (kbd "s-<down>") 'end-of-buffer)
;; (global-set-key (kbd "s-<up>") 'backward-paragraph)
;; (global-set-key (kbd "s-<down>") 'forward-paragraph)

(global-set-key (kbd "M-s-<up>") 'scroll-up-line)
(global-set-key (kbd "M-s-<down>") 'scroll-down-line)

;; (global-set-key (kbd "s-/") 'comment-or-uncomment-region)
(global-set-key (kbd "M-s-<backspace>") 'delete-trailing-whitespace)
					
;; duplicate the current line
(global-set-key (kbd "s-.") "\C-a\C- \C-n\M-w\C-y")

;; comment the whole current line
(global-set-key (kbd "s-/") 'comment-dwim-line)

;; delete other windows
;; (global-set-key (kbd "C-s-<268632075>") 'delete-other-windows)
(global-set-key (kbd "s-\\") 'delete-other-windows)

;; replacement
(global-set-key (kbd "s-r") 'query-replace)
