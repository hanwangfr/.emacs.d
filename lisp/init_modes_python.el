;;; ;;;;;;; Modified version of the configuration from
;; https://github.com/AndreaCrotti/minimal-emacs-configuration


;; ;; Python mode settings
(require 'python-mode)
(add-to-list 'auto-mode-alist '("\\.py$" . python-mode))
;; (setq py-electric-colon-active t)

(require 'cython-mode)

;; (eval-after-load 'python
;;   '(define-key python-mode-map (kbd "s-<") 'python-indent-shift-left))

;; (eval-after-load 'python
;;   '(define-key python-mode-map (kbd "s->") 'python-indent-shift-right))

;; Indentation for python-mode.el
(eval-after-load 'python-mode
  '(define-key python-mode-map (kbd "C-<") 'py-shift-block-left))

(eval-after-load 'python-mode
  '(define-key python-mode-map (kbd "C->") 'py-shift-block-right))

(eval-after-load 'python-mode
  '(define-key python-mode-map (kbd "C-,") 'py-shift-line-left))

(eval-after-load 'python-mode
  '(define-key python-mode-map (kbd "C-.") 'py-shift-line-right))

(eval-after-load 'python-mode
  '(define-key python-mode-map (kbd "<backtab>") 'py-dedent))


(add-hook 'python-mode-hook 'autopair-mode)
(add-hook 'python-mode-hook 'yas-minor-mode)
(add-hook 'python-mode-hook 'anaconda-mode)

;; (exec-path-from-shell-copy-env "PYTHONPATH")
;; (exec-path-from-shell-copy-env "PATH")

;; (setenv "PATH" (concat "/Users/hanwang/anaconda/bin" ":" (getenv "PATH"))) ;; environment problem: /usr/texbin is the path of latex command that must be manually loaded if emacs is started from the docker
;; (setenv "PYTHONPATH" "/usr/local/lib/python2.7/site-packages/:$HOME/.local/lib/python2.7/site-packages/:$HOME/Codes/Acwim")
;; (setq python-shell-virtualenv-path "/bin/bash:/Users/hanwang/anaconda/bin")
;; (setq python-shell-virtualenv-path (expand-file-name "~/.virtualenvs"))

;; ;; Jedi settings -- THIS DOESN'T WORK
;; (require 'jedi)
;; It's also required to run "pip install --user jedi" and "pip
;; install --user epc" to get the Python side of the library work
;; correctly.
;; With the same interpreter you're using.

;; if you need to change your python intepreter, if you want to change it
;; (setq jedi:server-command
;;       '("python" "/Users/hanwang/.emacs.d/elpa/jedi-core-20150623.2335/jediepcserver.py"))

;; ;; Don't know what this does
;; (require 'pungi)
;; (add-hook #'python-mode-hook
;;           '(lambda ()
;;              (pungi:setup-jedi)))

;; (add-hook 'python-mode-hook
;; 	  (lambda ()
;; 	    (jedi:setup)
;; 	    (jedi:ac-setup)
;;             ;; (local-set-key "\C-cd" 'jedi:show-doc)
;;             ;; (local-set-key (kbd "M-SPC") 'jedi:complete)
;;             ;; (local-set-key (kbd "M-.") 'jedi:goto-definition))
;; 	    )
;; 	  )


(add-hook 'python-mode-hook 'auto-complete-mode)
