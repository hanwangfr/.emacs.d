;; General keybindings for Linux and windows

;; For pure Emacs only
;; Host system (Ubuntu 14.04) uses Hyper as functional key, which is remapped to Control-Hyper. So do not use C-H combinations here.

;; Switch between windows      
(setq windmove-wrap-around t)
(global-set-key (kbd "H-M-<left>") 'windmove-left)          ; move to left windnow
(global-set-key (kbd "H-M-<right>") 'windmove-right)        ; move to right window
(global-set-key (kbd "H-M-<up>") 'windmove-up)              ; move to upper window
(global-set-key (kbd "H-M-<down>") 'windmove-down)          ; move to downer window

;; Scrolling
(global-set-key (kbd "H-S-<up>") 'scroll-up-line)
(global-set-key (kbd "H-S-<down>") 'scroll-down-line)

;; Edit
;; (global-set-key (kbd "M-<up>") 'beginning-of-visual-line)
;; (global-set-key (kbd "M-<down>") 'end-of-visual-line)
(global-set-key (kbd "M-<up>") 'backward-sentence)
(global-set-key (kbd "M-<down>") 'forward-sentence)
(global-set-key (kbd "M-<left>") 'left-word)
(global-set-key (kbd "M-<right>") 'right-word)

(global-set-key (kbd "H-<left>") 'back-to-indentation)
(global-set-key (kbd "H-<right>") 'move-end-of-line)

;; (global-set-key (kbd "C-<up>") 'backward-paragraph)
;; (global-set-key (kbd "C-<down>") 'forward-paragraph)

;; (global-set-key (kbd "H-/") 'comment-or-uncomment-region)
(global-set-key (kbd "H-<backspace>") 'delete-trailing-whitespace)

(global-set-key (kbd "C-<prior>") 'tabbar-backward-tab)
(global-set-key (kbd "C-<next>") 'tabbar-forward-tab)
(global-set-key (kbd "C-S-<prior>") 'tabbar-backward-group)
(global-set-key (kbd "C-S-<next>") 'tabbar-forward-group)

;; duplicate the current line
(global-set-key (kbd "H-.") "\C-a\C- \C-n\M-w\C-y")

;; comment the whole current line
(global-set-key (kbd "H-/") 'comment-dwim-line)

;; delete other windows
;; (global-set-key (kbd "C-s-<268632075>") 'delete-other-windows)
(global-set-key (kbd "H-\\") 'delete-other-windows)

(global-set-key (kbd "H-w") 'kill-buffer)
(global-set-key (kbd "H-s") 'save-buffer)
(global-set-key (kbd "S-H-s") 'save-some-buffers)
(global-set-key (kbd "H-a") 'mark-whole-buffer)
(global-set-key (kbd "H-z") 'undo)
(global-set-key (kbd "H-r") 'query-replace)






