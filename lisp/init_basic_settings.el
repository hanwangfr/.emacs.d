;;;;;;;;;;;;;;; Basic settings
;; Only light-weight settings and modes here. 

(setq inhibit-startup-message t
      inhibit-startup-echo-area-message t) ; No welcome message
;; (menu-bar-mode -1)
(tool-bar-mode -1) ; No tool bar

(delete-selection-mode t) ; overwrite on selections
(setq auto-revert-mode t) ; Auto-revert
(setq-default fill-column 80) ; Line wrapping
(add-hook 'text-mode-hook 'turn-on-auto-fill) 
(setq scroll-step 1) ;; keyboard scroll one line at a time
(global-visual-line-mode 1) ;; Visual line mode for all buffers

(setq frame-title-format
      (list ;(format "%s %%S: %%j " (system-name))
	    '(buffer-file-name "%f" (dired-directory dired-directory "%b")))) ; Show full file name on the title

(desktop-save-mode 1) ; restore opened files, 0 for off

(global-linum-mode t) ; Line and column number
(column-number-mode t) ; Line and column number

;;(setq visible-bell nil) ; turn off the beep
(setq ring-bell-function 'ignore)

;; alias
(defalias 'eb 'eval-buffer)
(defalias 'er 'eval-region)

(show-paren-mode t)
(setq confirm-nonexistent-file-or-buffer nil)
(fset 'yes-or-no-p 'y-or-n-p)

(setq ido-create-new-buffer 'always)

(setq kill-buffer-query-functions
      (remq 'process-kill-buffer-query-function
	    kill-buffer-query-functions))

;; (setq display-time-day-and-date 1)

;; (global-font-lock-mode 1)
;; (global-linum-mode 1)
;; (highlight-changes-mode 1)
;; (size-indication-mode 1)
(setq buffer-file-coding-system 'utf-8)

;; prevent emacs vc mode from launching when editing in a git repo
;; (setq vc-handled-backends ())

;; maybe dangerous, if we are editing DOS files?
;; (add-hook 'before-save-hook 'unix-newline)

(setq ispell-program-name "/usr/local/bin/ispell")

;; disable backup files and annoying auto-saving and symlinking
(setq make-backup-files nil) ;; '~' backup files
(setq auto-save-default nil) ;; dont auto-save
(setq create-lockfiles nil) ;; dont make symlink lock files

(winner-mode 1) ; undo changes on windows

(put 'downcase-region 'disabled nil)

(global-hl-line-mode 1) ;; Highlight the current line
;; (set-face-background 'hl-line nil)
;; (set-face-foreground 'highlight nil) 
;; (set-face-foreground 'hl-line nil)

;;;;;;;;;;;;;; Global keybindings
(global-set-key [f8] (lambda () (interactive) (find-file "~/.emacs.d/init.el")))
(global-set-key [f9] (lambda () (interactive) (find-file "~/.emacs.d/abbrev_defs.el")))

;;;;;;;;;;;;;;;;; Comment the whole current line
;; Original idea from
;; http://www.opensubscriber.com/message/emacs-devel@gnu.org/10971693.html
(defun comment-dwim-line (&optional arg)
  "Replacement for the comment-dwim command.
        If no region is selected and current line is not blank and we are not at the end of the line,
        then comment current line.
        Replaces default behaviour of comment-dwim, when it inserts comment at the end of the line."
  (interactive "*P")
  (comment-normalize-vars)
  (if (and (not (region-active-p)) (not (looking-at "[ \t]*$")))
      (comment-or-uncomment-region (line-beginning-position) (line-end-position))
    (comment-dwim arg)))
