;;-*-coding: utf-8;-*-
(define-abbrev-table 'Buffer-menu-mode-abbrev-table '())

(define-abbrev-table 'Custom-mode-abbrev-table '())

(define-abbrev-table 'Info-edit-mode-abbrev-table '())

(define-abbrev-table 'TeX-error-overview-mode-abbrev-table '())

(define-abbrev-table 'TeX-output-mode-abbrev-table '())

(define-abbrev-table 'apropos-mode-abbrev-table '())

(define-abbrev-table 'awk-mode-abbrev-table '())

(define-abbrev-table 'bibtex-mode-abbrev-table '())

(define-abbrev-table 'bookmark-bmenu-mode-abbrev-table '())

(define-abbrev-table 'bookmark-edit-annotation-mode-abbrev-table '())

(define-abbrev-table 'c++-mode-abbrev-table '())

(define-abbrev-table 'c-mode-abbrev-table '())

(define-abbrev-table 'comint-mode-abbrev-table '())

(define-abbrev-table 'completion-list-mode-abbrev-table '())

(define-abbrev-table 'conf-colon-mode-abbrev-table '())

(define-abbrev-table 'conf-javaprop-mode-abbrev-table '())

(define-abbrev-table 'conf-ppd-mode-abbrev-table '())

(define-abbrev-table 'conf-space-mode-abbrev-table '())

(define-abbrev-table 'conf-unix-mode-abbrev-table '())

(define-abbrev-table 'conf-windows-mode-abbrev-table '())

(define-abbrev-table 'conf-xdefaults-mode-abbrev-table '())

(define-abbrev-table 'custom-new-theme-mode-abbrev-table '())

(define-abbrev-table 'custom-theme-choose-mode-abbrev-table '())

(define-abbrev-table 'debugger-mode-abbrev-table '())

(define-abbrev-table 'display-time-world-mode-abbrev-table '())

(define-abbrev-table 'docTeX-mode-abbrev-table '())

(define-abbrev-table 'doctex-mode-abbrev-table '())

(define-abbrev-table 'edit-abbrevs-mode-abbrev-table '())

(define-abbrev-table 'eieio-custom-mode-abbrev-table '())

(define-abbrev-table 'emacs-lisp-byte-code-mode-abbrev-table '())

(define-abbrev-table 'emacs-lisp-mode-abbrev-table '())

(define-abbrev-table 'finder-mode-abbrev-table '())

(define-abbrev-table 'fundamental-mode-abbrev-table '())

(define-abbrev-table 'gdb-script-mode-abbrev-table '())

(define-abbrev-table 'global-abbrev-table
  '(
    ("0alt" "alternative" nil 0)
    ("0apprx" "approximation" nil 5)
    ("0bg" "background" nil 0)
    ("0char" "character" nil 0)
    ("0coef" "coefficient" nil 12)
    ("0decomp" "decomposition" nil 1)
    ("0def" "definition" nil 3)
    ("0dic" "dictionary" nil 0)
    ("0dim" "dimension" nil 0)
    ("0dir" "direction" nil 0)
    ("0env" "environment" nil 0)
    ("0eval" "evaluation" nil 0)
    ("0ex" "example" nil 0)
    ("0fn" "function" nil 17)
    ("0frq" "frequency" nil 3)
    ("0info" "information" nil 1)
    ("0intpl" "interpolation" nil 3)
    ("0intr" "introduce" nil 0)
    ("0intro" "introduction" nil 0)
    ("0ivp" "inverse problems" nil 3)
    ("0obs" "observation" nil 3)
    ("0pos" "position" nil 3)
    ("0prbl" "problem" nil 3)
    ("0proba" "probability" nil 3)
    ("0prop" "proposition" nil 3)
    ("0prs" "presentation" nil 3)
    ("0recon" "reconstruction" nil 0)
    ("0reprs" "representation" nil 3)
    ("0sol" "solution" nil 0)
    ("0sys" "system" nil 0)
    ("0var" "variable" nil 0)
    ("0vc" "vector" nil 1)
    ("0wvl" "wavelet" nil 3)
    ("1curly" "“\\([^”]+?\\)”" nil 0)
    ("1num" "\\([0-9]+?\\)" nil 0)
    ("1str" "\\([^\"]+?\\)\"" nil 0)
    ("bil" "bilinear" nil 0)
   ))

(define-abbrev-table 'gud-mode-abbrev-table '())

(define-abbrev-table 'help-mode-abbrev-table '())

(define-abbrev-table 'idl-mode-abbrev-table '())

(define-abbrev-table 'inferior-python-mode-abbrev-table '())

(define-abbrev-table 'java-mode-abbrev-table '())

(define-abbrev-table 'jython-mode-abbrev-table '())

(define-abbrev-table 'latex-mode-abbrev-table '())

(define-abbrev-table 'lisp-mode-abbrev-table '())

(define-abbrev-table 'matlab-mode-abbrev-table '())

(define-abbrev-table 'matlab-shell-help-mode-abbrev-table '())

(define-abbrev-table 'matlab-shell-topic-mode-abbrev-table '())

(define-abbrev-table 'message-mode-abbrev-table '())

(define-abbrev-table 'messages-buffer-mode-abbrev-table '())

(define-abbrev-table 'objc-mode-abbrev-table '())

(define-abbrev-table 'occur-edit-mode-abbrev-table '())

(define-abbrev-table 'occur-mode-abbrev-table '())

(define-abbrev-table 'outline-mode-abbrev-table '())

(define-abbrev-table 'package-menu-mode-abbrev-table '())

(define-abbrev-table 'pike-mode-abbrev-table '())

(define-abbrev-table 'plain-tex-mode-abbrev-table '())

(define-abbrev-table 'process-menu-mode-abbrev-table '())

(define-abbrev-table 'prog-mode-abbrev-table '())

(define-abbrev-table 'python-mode-abbrev-table
  '(
    ("class" "" python-skeleton-class 0)
    ("def" "" python-skeleton-def 0)
    (#("for" 0 3 (face font-lock-keyword-face fontified t)) "" python-skeleton-for 0)
    ("if" "" python-skeleton-if 0)
    ("try" "" python-skeleton-try 0)
    ("while" "" python-skeleton-while 0)
   ))

(define-abbrev-table 'python2-mode-abbrev-table '())

(define-abbrev-table 'python3-mode-abbrev-table '())

(define-abbrev-table 'reftex-select-bib-mode-abbrev-table '())

(define-abbrev-table 'reftex-select-label-mode-abbrev-table '())

(define-abbrev-table 'sh-mode-abbrev-table '())

(define-abbrev-table 'shell-mode-abbrev-table '())

(define-abbrev-table 'slitex-mode-abbrev-table '())

(define-abbrev-table 'snake-mode-abbrev-table '())

(define-abbrev-table 'snippet-mode-abbrev-table '())

(define-abbrev-table 'special-mode-abbrev-table '())

(define-abbrev-table 'tabulated-list-mode-abbrev-table '())

(define-abbrev-table 'tar-mode-abbrev-table '())

(define-abbrev-table 'tetris-mode-abbrev-table '())

(define-abbrev-table 'tex-mode-abbrev-table '())

(define-abbrev-table 'tex-shell-abbrev-table '())

(define-abbrev-table 'text-mode-abbrev-table '())

(define-abbrev-table 'url-cookie-mode-abbrev-table '())

(define-abbrev-table 'vc-git-log-edit-mode-abbrev-table '())

(define-abbrev-table 'vc-git-log-view-mode-abbrev-table '())

(define-abbrev-table 'vc-svn-log-view-mode-abbrev-table '())

